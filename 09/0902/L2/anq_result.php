<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//初期化関数を呼び出す
init();

//ログイン状態をチェックする関数を呼び出す
loginCheck();

if (isset($_GET["mode"]) && $_GET["mode"] == "new") {
	$_SESSION["keyword"] = "";
	$_SESSION["sex_key"] = "";
	$_SESSION["age_key"] = "";
	$_SESSION["where"] = "";
}

//データベースに接続する関数を呼び出す
$db = db_connect();

// 削除ボタンが押されたときの処理
if (isset($_POST["del_id"]) && is_array($_POST["del_id"])) {
	// POST値をすべて$db->quoteSmartでエスケープして同時にシングルクォートで囲みます
	foreach ($_POST["del_id"] as $del_id) {
		if (!is_numeric($del_id)) continue;
		$del_id_list[] = $db->quoteSmart($del_id);
	}
	
	// カンマ区切りにする
	$where_id = implode(",", $del_id_list);
	
	$sql = "UPDATE anq_t SET del_flag = '1' WHERE anq_id IN ({$where_id})";
	$res = $db->query($sql);
	if ( DB::isError($res) ) {
		print "エラーが発生しました。<br />操作をやりなおしてください";
		exit;
	}
}


//条件文をセットする変数
$where = "";

//検索ボタンをクリックしたときの処理
if (isset($_POST["search"])) {
	//検索条件をセット
	$keyword    = $_POST["keyword"];
	$sexkeyword = $_POST["sex_key"];
	$agekeyword = $_POST["age_key"];

	//条件文を作成するための一時変数
	$searchwhere = "";

	//キーワードが入力されていたら検索文を作成
	// 検索キーワードなどの検索する値は、PEARのDBライブラリにあるquoteSmartメソッドを使用して、SQLで使用してはいけない文字列をエスケープします。
	if ($keyword != "") {
		// LIKE句に掛ける、%と_の特殊文字をエスケープする
		$keyword = str_replace('%', '\%', $keyword);
		$keyword = str_replace('_', '\_', $keyword);
		$keyword    = $db->quoteSmart("%" . $keyword . "%");
		$searchwhere .= " (name LIKE {$keyword} OR comment LIKE {$keyword}) AND";
	}
	//性別が選択されていたら検索文を作成
	if ($sexkeyword != "") {
		$sexkeyword = $db->quoteSmart($sexkeyword);
		$searchwhere .= " sex = {$sexkeyword} AND";
	}
	//年代が選択されていたら検索文を作成
	if ($agekeyword != "") {
		$agekeyword = $db->quoteSmart($agekeyword);
		$searchwhere .= " age = {$agekeyword} AND";
	}
	
	//入力した内容をセッションに保存する
	$_SESSION["keyword"] = isset($_POST["keyword"]) ? $_POST["keyword"] : "";
	$_SESSION["sex_key"] = isset($_POST["sex_key"]) ? $_POST["sex_key"] : "";
	$_SESSION["age_key"] = isset($_POST["age_key"]) ? $_POST["age_key"] : "";
	
	//条件文がセットされているか確認する
	if ($searchwhere != "") {
		$_SESSION["where"] = "WHERE" . $searchwhere . " del_flag != '1'";
	} else {
		$_SESSION["where"] = "WHERE del_flag != '1'";
	}
	
}

//条件文があったらセットする
if (isset($_SESSION["where"]) && $_SESSION["where"] != "") {
	$where = $_SESSION["where"];
} else {
	$where = "WHERE del_flag != '1'";
}

//条件を付加したアンケートデータを取得する
$sql = "SELECT * FROM anq_t {$where} ORDER BY create_datetime DESC";
$data = pager_search ($sql,$db);

//好きな動物の「,」区切りのデータを配列データに変換する処理を行う
foreach ((array)$data["data"] as $key => $value ) {
	$data["data"][$key]["animal"] = explode(",",$value["animal"]);
}

//Smartyを生成
$smarty = new MySmarty();
$smarty->assign("anq_list",$data["data"]);
$smarty->assign("links",$data["links"]);
$smarty->assign("sex_value",getSexList());
$smarty->assign("age_value",getAgeList());
$smarty->assign("animal_value",getAnimalList());
$smarty->display("admin/anq_result.tpl");
?>
<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

// 正しく入力されていないと確認画面へは遷移してくることができない
if (error_check($_SESSION)) {
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/input.php";
	header("Location: " . $url);
	exit;
}

//以下にSmartyの処理を追記します。

?>
/* 自動で倒したピンの得点計算をする部分 */
$sum = 0;	//合計得点
$frame_cnt = array();	//frameごとの得点
$strike_flg= array();

//1ゲームから10ゲームまで繰り返す
for ($i=1;$i<=10;$i++) {
	$nowpin = 10;
	$cnt = 0;
	for ($j=1;$j<=3;$j++) {
		//10ゲーム以外は3投目はないので、繰り返し終了
		if ($j == 3 && $i != 10) {break;}

		//値が空の場合は次の値へ
		if (!isset($result[$i][$j])) {continue;}

		$nowpin = $nowpin-$result[$i][$j];
		$sum += $result[$i][$j];
		//ストライクの場合
		if ($nowpin == 0 && $j == 1 && $i != 10) {
			$cnt = 2;
		} elseif ($nowpin == 0 && $j == 2 && $i != 10) {
			$cnt = 1;
		}
		//スペア・ストライクの場合は、次の投球の値を足す処理をする
		if ($cnt >= 1) {
			for ($k=$i+1;$k<=10;$k++) {
				for ($l=1;$l<=3;$l++) {
					if (!isset($result[$k][$l])) {continue;}
					if ($l == 3 || $cnt <= 0) {break;}
						$sum += $result[$k][$l];
						$cnt--;
				}
			}
			break;
		}
	}
	$frame_cnt[$i] = $sum;
}
/* 自動で倒したピンの得点計算をする部分 ここまで */

//円Graph用のGraphオブジェクトを生成します。
$graph = new PieGraph($width,200,"auto");

//Graphに影をセットします。
$graph->SetShadow();

//Graphのフォントとタイトルをセットします
$graph->title->Set($title);
$graph->title->SetFont(FF_MINCHO, FS_NORMAL, 12);

//レジェンド（説明）のフォントをセットします
$graph->legend->SetFont(FF_MINCHO, FS_NORMAL);

//円グラフのオブジェクトを生成します。（Y軸のデータを挿入する
$p1 = new PiePlot($datay);

//円Graphのテーマをセット
$p1->SetTheme("earth");

//円の中心の位置を指定します
$p1->SetCenter(0.4);

//レジェンド（説明）をセットします
$p1->SetLegends($datax);

//Graphに円Graphを追加します。
$graph->Add($p1);

// Graphを表示します
$graph->Stroke();

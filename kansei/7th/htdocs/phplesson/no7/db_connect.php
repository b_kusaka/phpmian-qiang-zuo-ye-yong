<?php
/* PEARのDBライブラリを読み込む */
require_once("DB.php");

// データベースとの接続情報を記述
$dsn = "mysql://dbuser:pass@localhost/phplesson";

//データベースへ接続
$db =& DB::connect($dsn);
if (DB::isError($db)) {
	print "ConnectError!";
	exit;
} else {
	print "DataBase Connect OK!";
	exit;
}
?>
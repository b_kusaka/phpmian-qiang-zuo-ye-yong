<?php
//Smartyクラスの呼び出し
require_once("../../../libs/Smarty.class.php");

//変数をセット
$sex = "m";

//Smartyインスタンス生成
$smarty = new Smarty();

//テンプレート変数の割り当て
$smarty->assign("sex", $sex);

//テンプレートの表示
$smarty->display("smarty_if.tpl");

?>
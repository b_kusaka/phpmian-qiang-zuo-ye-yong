<?php

function init() {
	//MySmartyクラスの読み込み
	require_once($_SERVER["DOCUMENT_ROOT"]. "/../libs/MySmarty.class.php");
	
	//セッションを開始する
	session_start();
	session_regenerate_id(true);

}

//性別のリスト（配列）を返す関数
function getSexList() {
	$sex_value = array(
		"1" => "男性",
		"2" => "女性"
	);
	return $sex_value;
}

//年代のリスト（配列）を返す関数
function getAgeList() {
	$age_value = array(
		"1" => "10代",
		"2" => "20代",
		"3" => "30代",
		"4" => "40代",
		"5" => "50代",
		"6" => "60代以上"
	);
	return $age_value;
}

//動物のリスト（配列）を返す関数
function getAnimalList() {
	$animal_value = array(
		"1" => "いぬ",
		"2" => "ねこ",
		"3" => "らいおん",
		"4" => "きりん",
		"5" => "とら",
		"6" => "うさぎ",
		"7" => "さる",
		"8" => "ぺんぎん",
		"9" => "うま",
		"10" => "ぞう"
	);
	return $animal_value;
}

//自作エラーチェック関数
function error_check($check_data) {
	
	//エラーメッセージを格納する配列変数を初期化
	$error_list = array();

	//名前のエラーチェック

	//空白文字を削除して、名前が空の場合はエラーメッセージをセット
	if (isset($check_data["name"]) && trim($check_data["name"]) === "") {
		$error_list[] = "名前を入力してください。";
	//名前の文字列が100文字を超えたらエラーメッセージをセット
	} elseif (mb_strlen($check_data["name"]) > 100) {
		$error_list[] = "名前は100文字以内で入力してください。";
	}
	
	//性別のエラーチェック
	
	//性別の変数が存在しない場合はエラーメッセージをセット
	if (!isset($check_data["sex"])) {
		$error_list[] = "性別を選択してください。";
	//性別の配列を取得し、選択した値が性別の配列のキーに存在しない場合はエラーメッセージをセット
	} elseif (!array_key_exists($check_data["sex"], getSexList())) {
		$error_list[] = "正しい性別を選択してください。";
	}

	//年代のエラーチェック
	
	//年代の変数が入力されている場合はエラーチェックを行なう
	if (isset($check_data["age"]) && $check_data["age"] !== "") {
		//年代の配列を取得し、選択した値が年代の配列のキーに存在しない場合はエラーメッセージをセット
		if (!array_key_exists($check_data["age"], getAgeList())) {
			$error_list[] = "正しい年代を選択してください。";
		}
	}
	
	//好きな動物のエラーチェック

	//好きな動物をひとつも選択しなかった場合はエラーメッセージをセット
	if (!isset($check_data["animal"]) || !is_array($check_data["animal"])) {
		$error_list[] = "好きな動物は最低1つを選択してください。";
	} else {
		//好きな動物は配列で値をPOSTするため、foreachで展開する
		foreach ($check_data["animal"] as $check_value) {
			//好きな動物の配列を取得し、選択した値が好きな動物の配列のキーに存在しない場合はエラーメッセージをセット
			if (!array_key_exists($check_value, getAnimalList())) {
				$error_list[] = "正しい動物を選択してください。";
				break;
			}
		}
	}
	
	//コメントのエラーチェック

	//空白文字を削除して、コメントが空の場合はエラーメッセージをセット
	if (isset($check_data["comment"]) && trim($check_data["comment"]) === "") {
		$error_list[] = "コメントを入力してください。";
	//コメントの文字列が1000文字を超えたらエラーメッセージをセット
	} elseif (mb_strlen($check_data["comment"]) > 1000) {
		$error_list[] = "コメントは1000文字以内で入力してください。";
	}

	//エラーメッセージリストを返す
	return $error_list;

}

//DBに接続する関数
function db_connect() {
	require_once("DB.php");

	// データベースとの接続情報を記述
	$dsn = "mysql://dbuser:pass@localhost/phplesson";

	//データベースに接続する
	$db = DB::connect($dsn);

	//接続情報にエラーがないか確認
	if (PEAR::isError($db)) {
		print "DB Connect Error";
		die($db->getMessage());
	}
	//接続されたオブジェクトを返す
	return $db;
}

?>
<?php
/* PEARのHTTP_Requestライブラリを読み込む */
require_once("HTTP/Request.php");

//HTTP_Requestライブラリを作成
$req = new HTTP_Request("http://www.yahoo.co.jp/");

//取得できたかを確認する
//PEAR::isError()は エラーオブジェクトを返した場合はtrueを返します
if (!PEAR::isError($req->sendRequest())) {
	//ページの内容を取得して、表示する
	print $req->getResponseBody();
}

?>
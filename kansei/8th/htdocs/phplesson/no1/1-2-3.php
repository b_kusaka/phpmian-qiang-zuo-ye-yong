<?php
$version = substr(PHP_VERSION, 0, 1);	//PHPのローカルバージョンから先頭の１文字を切り出し

//切り出した先頭１文字を比較する
switch ($version) {
  case 6:
    print 'PHPのバージョンは6です';
    break;
  case 5:
    print 'PHPのバージョンは5です';
    break;
  case 4:
    print 'PHPのバージョンは4です';
    break;
  case 3:
    print 'PHPのバージョンは3です';
    break;
  default:
    print 'PHPのバージョン3～6に該当しません';
}
?>

<?php
	//ボーリングの得点結果を格納する配列
	$result = array();
	$pin = 10;	//ピンの数を保持
	$flag = 0;	//10フレームの3投目判定用のフラグ
	
	//1ゲームから10ゲームまで繰り返す
	for ($i = 1; $i <= 10; $i++) {
		$end_flg = 0;	//終了を判断するためのフラグ
		for ($j = 1; $j <= 3; $j++) {
			if ($j == 1) {
				$nowpin = $pin;
			}
			//10ゲームの処理を分ける
			if ($i == 10) {
				$pin_out = rand(0,$nowpin);
				$result[$i][$j] = $pin_out;
				$nowpin = $nowpin - $pin_out;
				//10本倒していたら、10本セットして、3投目用のフラグを1にする。
				if ($nowpin == 0) {
					$nowpin = $pin;
					$flag = 1;
				}
				//2投内でストライクまたはスペアが出ていない場合は3投目はないので処理終了
				if($j == 2 && $flag == 0) {
					break;
				}
			} else {
				//10ゲーム以外は3投目はないので、繰り返し終了
				if ($j == 3) {
					break;
				}
				$pin_out = rand(0,$nowpin);
				$result[$i][$j] = $pin_out;
				$nowpin = $nowpin - $pin_out;
				//ストライクの判定
				if ($nowpin == 0) {
					break;
				}
			}
		}
	}


$sum = 0;	//合計得点
$frame_cnt = array();	//frameごとの得点
$strike_flg= array();

//1ゲームから10ゲームまで繰り返す
for ($i=1;$i<=10;$i++) {
	$nowpin = 10;
	$cnt = 0;
	for ($j=1;$j<=3;$j++) {
		//10ゲーム以外は3投目はないので、繰り返し終了
		if ($j == 3 && $i != 10) {break;}

		//値が空の場合は次の値へ
		if (!isset($result[$i][$j])) {continue;}

		$nowpin = $nowpin-$result[$i][$j];
		$sum += $result[$i][$j];
		//ストライクの場合
		if ($nowpin == 0 && $j == 1 && $i != 10) {
			$cnt = 2;
		} elseif ($nowpin == 0 && $j == 2 && $i != 10) {
			$cnt = 1;
		}
		//スペア・ストライクの場合は、次の投球の値を足す処理をする
		if ($cnt >= 1) {
			for ($k=$i+1;$k<=10;$k++) {
				for ($l=1;$l<=3;$l++) {
					if (!isset($result[$k][$l])) {continue;}
					if ($l == 3 || $cnt <= 0) {break;}
						$sum += $result[$k][$l];
						$cnt--;
				}
			}
			break;
		}
	}
	$frame_cnt[$i] = $sum;
}


print <<< EOF
<html>
<head>
<title></title>
</head>
<body>
<h2>ボーリング自動得点計算</h2>
<h4>自動で１０フレームを行い、その得点を割り出すプログラムです。</h4>
<table border="1">
EOF;
//フレーム数を表示する（10フレーム
print "<tr>";
for ($i=1;$i<=10;$i++) {
	$col = ($i==10) ? 3 : 2;
	print "<td colspan=\"{$col}\">{$i}Frame</td>";
}
print "</tr>";

//フレームの投球ごとの得点を表示する（10フレーム * 3投
print "<tr>";
for ($i=1;$i<=10;$i++) {
	//最初のピン数
	$pin=10;
	for ($j=1;$j<=3;$j++) {
		if ($i!=10 && $j==3) {break;}

		//値が空の場合は次の値へ
		if (!isset($result[$i][$j])) {continue;}
		
		//ピンの数から倒したピン数を引く
		$pin = $pin - $result[$i][$j];

		//10フレーム以外の処理
		if ($i != 10) {
			//スペアの場合は表示を記号にする
			if ($pin == 0 && $j == 2) {
				print "<td>／</td>";

			//ストライクの場合は表示を記号にする
			} elseif ($pin == 0 && $j == 1 ) {
				print "<td colspan=\"2\">×</td>";
				break;

			//その他の場合は倒したピン数を表示
			} else {
				print "<td>{$result[$i][$j]}</td>";
			}
		//10フレームのときの処理
		} else {
			//連続で10ピン倒したときはストライクの記号を表示する
			if ($pin == 0 && $flag == 1) {
				print "<td>×</td>";
				//10ピンをセットする
				$pin=10;
			//2投目か3投目でピンの数が0の場合はスペアの記号を表示する
			} elseif ($pin == 0 && ($j == 2 or $j == 3)) {
				print "<td>／</td>";
				$pin=10;
				$flag = 1;
			//初球でストライクの場合はストライクの記号を表示する
			} elseif ($pin == 0 && $j == 1 ) {
				print "<td>×</td>";
				//10ピンセットしてフラグを1にする。
				$pin=10;
				$flag = 1;
			//その他の場合は得点をそのまま表示する
			} else {
				print "<td>{$result[$i][$j]}</td>";
				$flag = 0;
			}
		
		}
	}
}
print "</tr>";

//フレームでの得点を表示する（10フレーム分
print "<tr>";
for ($i=1;$i<=10;$i++) {
	$col = ($i==10) ? 3 : 2;
	print "<td colspan=\"{$col}\">{$frame_cnt[$i]}</td>";

}
print "</tr>";

print <<< EOF
</table>
</body>
</html>
EOF;

?>
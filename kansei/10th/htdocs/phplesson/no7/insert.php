<?php
//PEARのDBライブラリを読み込む
require_once("DB.php");

// データベースとの接続情報を記述
$dsn = "mysql://dbuser:pass@localhost/phplesson";

//データベースへ接続
$db =& DB::connect($dsn);
if (DB::isError($db)) {
	print "ConnectError!";
	exit;
}

//address_tテーブルへデータを追加する
$sql = "INSERT INTO address_t VALUES ('9','三浦','1981-07-05','東京都新宿区','説明説明','miura@hogehoge.hoge')";

//DBのqueryメソッドを使用してSQL文を実行します
$res = $db->query($sql);

//何行のデータが反映したのかを取得するメソッドです
$num = $db->affectedRows();

//反映された数を表示する
print $num . "rows Update";

?>
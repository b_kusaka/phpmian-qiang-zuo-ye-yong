<?php

// 名前がPOSTされたら
if (isset($_POST["username"])) {
	// 入力された名前をクッキーにセットする。但し有効期限は120秒（2分）
	setcookie("username", $_POST["username"], time()+120);
	
	print "お名前をクッキーに保存しました。<br />";
	print "2分以内に再度cookie.phpにアクセスしてください。";
}

?>
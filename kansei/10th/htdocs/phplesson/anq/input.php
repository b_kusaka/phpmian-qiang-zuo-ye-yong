<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

//エラーメッセージを受け取る変数の初期化
$error_list = array();

// 登録確認ボタンがクリックされたとき
if (isset($_POST["confirm"])) {
	// アンケートデータをセッションに格納する
	$_SESSION["name"]    = isset($_POST["name"])    ? $_POST["name"]    : "";
	$_SESSION["sex"]     = isset($_POST["sex"])     ? $_POST["sex"]     : "";
	$_SESSION["age"]     = isset($_POST["age"])     ? $_POST["age"]     : "";
	$_SESSION["animal"]  = isset($_POST["animal"])  ? $_POST["animal"]  : "";
	$_SESSION["comment"] = isset($_POST["comment"]) ? $_POST["comment"] : "";
	
	// エラーチェック関数の呼び出し、エラーメッセージを受け取る
	$error_list = error_check($_SESSION);

	//エラーがなければ、確認画面へ遷移する
	if (!$error_list) {
		$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/confirm.php";
		header("Location: " . $url);
		exit;
	}
}

/* ここからSmartyの処理を追加 */

//MySmartyクラスを生成する
$smarty = new MySmarty();

//性別リストと年齢リストと動物リストの配列をassignする
$smarty->assign("sex_value",getSexList());
$smarty->assign("age_value",getAgeList());
$smarty->assign("animal_value",getAnimalList());

//エラーメッセージリストをassignする
$smarty->assign("error_list",$error_list);

//入力フォームを表示する
$smarty->display("input.tpl");

?>
{$message}<br />
{* 先頭を大文字に修飾して出力 *}
{$message|capitalize}<br />

{$today}<br />
{* 日付を日本語表記に修飾して出力 *}
{$today|date_format:"%Y年%m月 %d日"}<br />

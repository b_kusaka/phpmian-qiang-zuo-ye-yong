<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/base.css" rel="stylesheet" type="text/css" />
<title>グラフ一覧画面</title>
</head>

<body>
<div class="sub-title">
グラフ一覧
</div>
<table class="editform">
	<tr>
		<td>性別の棒グラフ表示<br /><img src="./line_graph_sex.php"></td>
	</tr>
	<tr>
		<td>性別の円グラフ表示<br /><img src="./circle_graph_sex.php"></td>
	</tr>
	<tr>
		<td>年別の棒グラフ表示<br /><img src="./line_graph_age.php"></td>
	</tr>
	<tr>
		<td>年別の円グラフ表示<br /><img src="./circle_graph_age.php"></td>
	</tr>
	<tr>
		<td>好きな動物の棒グラフ表示<br /><img src="./line_graph_animal.php"></td>
	</tr>
	<tr>
		<td>好きな動物の円グラフ表示<br /><img src="./circle_graph_animal.php"></td>
	</tr>
</table>
</body>
</html>
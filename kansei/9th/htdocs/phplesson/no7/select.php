<?php
//PEARのDBライブラリを読み込む
require_once("DB.php");

// データベースとの接続情報を記述
$dsn = "mysql://dbuser:pass@localhost/phplesson";

//データベースへ接続
$db =& DB::connect($dsn);
if (DB::isError($db)) {
	print "ConnectError!";
	exit;
}

print "<html>";
print "<body>";
print "<table border=1>";
print "<tr><td>番号</td><td>名前</td><td>生年月日</td><td>住所</td><td>説明</td><td>メールアドレス</td></tr>";

//SELECTのSQL文
$sql = "SELECT * FROM address_t";

//SQL文を実行
$res = $db->query($sql);

//$res->fetchRow() : データを1行取得する
//フィールド順に数値が配列のキーとなって$rowに行のデータを取得する
while(is_array($row = $res->fetchRow(DB_FETCHMODE_ASSOC))) {
	//それぞれのフィールド名を連想配列のキーとしてのデータを取得できる
	print "<tr><td>" . $row["number"] . "</td>";
	print "<td>" . $row["name"] . "</td>";
	print "<td>" . $row["birthday"] . "</td>";
	print "<td>" . $row["address"] . "</td>";
	print "<td>" . $row["explanation"] . "</td>";
	print "<td>" . $row["mail"] . "</td></tr>";
}


print "</table>";
print "</body>";
print "</html>";


?>
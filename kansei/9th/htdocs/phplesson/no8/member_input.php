<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");
	
//初期化を行なう
init();

//エラーメッセージや登録メッセージを保持する
$mes = array();

//登録ボタンをクリックされたときの処理
if (isset($_POST["regist"])) {
	$name     = $_POST["name"];
	$login_id = $_POST["login_id"];
	$password = $_POST["password"];
	
	//データベースへ接続する
	$db = db_connect();

	/* エラーチェックを行なう */

	//名前のエラーチェックを行なう
	if ($name == "") {
		$mes[] = "名前を入力してください。";
	} elseif (mb_strlen($name) > 128) {
		$mes[] = "名前は128文字未満で入力してください。";
	}

	//ログインIDのエラーチェックを行なう
	if ($login_id == "") {
		$mes[] = "ログインIDを入力してください。";
	} elseif (strlen($login_id) != mb_strlen($login_id)){
		$mes[] = "ログインIDには半角英数字で入力してください。";
	} else{
		//同じIDが既に登録されていないかチェックをする
		$sql = "SELECT COUNT(*) FROM member_t WHERE login_id = '{$login_id}' and del_flag = '0'";
		$login_id_count = $db->getOne($sql);
		if ($login_id_count != "0") {
			$mes[] = "入力されたログインIDはすでに使用されているため登録できません。";
		}
	}

	//パスワードのエラーチェックを行なう
	if ($password == "") {
		$mes[] = "パスワードを入力してください。";
	} elseif (strlen($password) != mb_strlen($password)){
		$mes[] = "パスワードには半角英数字で入力してください。";
	}

	/* エラーチェックここまで */

	//エラーメッセージが格納されていない場合は登録する
	if (!count($mes)) {
		// PEARのDBライブラリにあるquoteSmartメソッドを使用して、SQLで使用してはいけない文字列をエスケープします。
		$name      = $db->quoteSmart($name);
		$login_id  = $db->quoteSmart($login_id);
		$password  = md5($password);
		
		//削除フラグは初期値の'0'を登録する
		$del_flag = "0";
		
		//データの登録を行なう
		// エスケープした値には「'」が付加されるので、「'」で囲っているのをはずします。
		$sql = "INSERT INTO member_t(name,login_id,password,del_flag,create_datetime)values({$name},{$login_id},'{$password}','{$del_flag}',now())";
		$res = $db->query($sql);
		if ( !DB::isError($res) ) {
			$mes[] = "データの登録を行ないました。";
		} else {
			$mes[] = "データの登録に失敗しました。";
		}
		
	}
}

//Smartyを生成する
$smarty = new MySmarty();

//メッセージを送る
$smarty->assign("mes", $mes);

//ページを表示する
$smarty->display("member_input.tpl");
?>
<?php
//Smartyクラスの呼び出し
require_once("../../../libs/Smarty.class.php");

//メッセージと日付のデータを変数にセット
$message = "hello smarty!";
$today   = "2007-01-01 10:05:45";

//Smartyインスタンス生成
$smarty = new Smarty();

//テンプレート変数の割り当て
$smarty->assign("message", $message);
$smarty->assign("today", $today);

//テンプレートの表示
$smarty->display("smarty_modifire.tpl");

?>
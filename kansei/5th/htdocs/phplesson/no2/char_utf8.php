<?php
	//PHP文字の出力をUTF-8にする設定項目を変更する（このPHPスクリプト内だけ有効な設定
	mb_internal_encoding ("UTF-8");
	mb_http_output("UTF-8");
	
	ob_start("mb_output_handler");
	header("Content-Type: text/html; charset=UTF-8");

	print "日本語を出力する";
?>
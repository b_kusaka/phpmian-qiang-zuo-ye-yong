<?php

function init() {
	//MySmartyクラスの読み込み
	require_once($_SERVER["DOCUMENT_ROOT"]. "/../libs/MySmarty.class.php");
	
	//セッションを開始する
	session_start();
	session_regenerate_id(true);

}

//性別のリスト（配列）を返す関数
function getSexList() {
	$sex_value = array(
		"1" => "男性",
		"2" => "女性"
	);
	return $sex_value;
}

//年代のリスト（配列）を返す関数
function getAgeList() {
	$age_value = array(
		"1" => "10代",
		"2" => "20代",
		"3" => "30代",
		"4" => "40代",
		"5" => "50代",
		"6" => "60代以上"
	);
	return $age_value;
}

//動物のリスト（配列）を返す関数
function getAnimalList() {
	$animal_value = array(
		"1" => "いぬ",
		"2" => "ねこ",
		"3" => "らいおん",
		"4" => "きりん",
		"5" => "とら",
		"6" => "うさぎ",
		"7" => "さる",
		"8" => "ぺんぎん",
		"9" => "うま",
		"10" => "ぞう"
	);
	return $animal_value;
}

//自作エラーチェック関数
function error_check($check_data) {
	
	//エラーメッセージを格納する配列変数を初期化
	$error_list = array();

	//名前のエラーチェック

	//空白文字を削除して、名前が空の場合はエラーメッセージをセット
	if (isset($check_data["name"]) && trim($check_data["name"]) === "") {
		$error_list[] = "名前を入力してください。";
	//名前の文字列が100文字を超えたらエラーメッセージをセット
	} elseif (mb_strlen($check_data["name"]) > 100) {
		$error_list[] = "名前は100文字以内で入力してください。";
	}
	
	//性別のエラーチェック
	
	//性別の変数が存在しない場合はエラーメッセージをセット
	if (!isset($check_data["sex"])) {
		$error_list[] = "性別を選択してください。";
	//性別の配列を取得し、選択した値が性別の配列のキーに存在しない場合はエラーメッセージをセット
	} elseif (!array_key_exists($check_data["sex"], getSexList())) {
		$error_list[] = "正しい性別を選択してください。";
	}

	//年代のエラーチェック
	
	//年代の変数が入力されている場合はエラーチェックを行なう
	if (isset($check_data["age"]) && $check_data["age"] !== "") {
		//年代の配列を取得し、選択した値が年代の配列のキーに存在しない場合はエラーメッセージをセット
		if (!array_key_exists($check_data["age"], getAgeList())) {
			$error_list[] = "正しい年代を選択してください。";
		}
	}
	
	//好きな動物のエラーチェック

	//好きな動物をひとつも選択しなかった場合はエラーメッセージをセット
	if (!isset($check_data["animal"]) || !is_array($check_data["animal"])) {
		$error_list[] = "好きな動物は最低1つを選択してください。";
	} else {
		//好きな動物は配列で値をPOSTするため、foreachで展開する
		foreach ($check_data["animal"] as $check_value) {
			//好きな動物の配列を取得し、選択した値が好きな動物の配列のキーに存在しない場合はエラーメッセージをセット
			if (!array_key_exists($check_value, getAnimalList())) {
				$error_list[] = "正しい動物を選択してください。";
				break;
			}
		}
	}
	
	//コメントのエラーチェック

	//空白文字を削除して、コメントが空の場合はエラーメッセージをセット
	if (isset($check_data["comment"]) && trim($check_data["comment"]) === "") {
		$error_list[] = "コメントを入力してください。";
	//コメントの文字列が1000文字を超えたらエラーメッセージをセット
	} elseif (mb_strlen($check_data["comment"]) > 1000) {
		$error_list[] = "コメントは1000文字以内で入力してください。";
	}

	//エラーメッセージリストを返す
	return $error_list;

}

//DBに接続する関数
function db_connect() {
	require_once("DB.php");

	// データベースとの接続情報を記述
	$dsn = "mysql://dbuser:pass@localhost/phplesson";

	//データベースに接続する
	$db = DB::connect($dsn);

	//接続情報にエラーがないか確認
	if (PEAR::isError($db)) {
		print "DB Connect Error";
		die($db->getMessage());
	}
	//接続されたオブジェクトを返す
	return $db;
}

//ログイン認証を行なう関数（ログインIDとパスワード
function Authenticator($loginid , $password) { 

	//データベースに接続する関数を呼び出す
	$db=db_connect();

	// PEARのDBライブラリにあるquoteSmartメソッドを使用して、SQLで使用してはいけない文字列をエスケープします。
	//member_tテーブルのpasswordフィールドはMD5関数で暗号化された値が格納されています。
	$q_loginid  = $db->quoteSmart($loginid);
	$q_password  = md5($password);
	
	//member_tにログインIDと暗号化したパスワードで認証データがあるかチェックするSQLを発行しています
	//エスケープログインIDの値には「'」が付加されるので、「'」で囲っているのをはずします。
	$sql = "
	SELECT * 
	FROM member_t 
	WHERE login_id = {$q_loginid} 
	AND password = '{$q_password}' 
	AND del_flag = '0'";
	
	//PEARのDBライブラリのqueryメソッドを使用して、ログインチェックするSQLを実行しています
	$res = $db->query($sql);

	if ( !DB::isError($res) ) {
		//「DB_FETCHMODE_ASSOC」はSQLの結果を連想配列形式で$rowに取得しています
		$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
	} else {
		return false;
	}

	//実行したSQLの結果で、ログインIDが格納されていた場合はtrue ない場合はfalseを返す
	if ($row["login_id"] != "") {
		return true;
	} else {
		return false;
	}
} 

//ログイン状態をチェックする関数
function loginCheck() {
	//「!isset($_SESSION['login_name'])」部分のisset関数は変数が使用されている場合はtrueを返し、
	//使用されていない場合はfalseを返す関数です。
	if (!isset($_SESSION['login_name'])) {
		$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/login.php";
		header("Location: " . $url);
		exit;
	}
	return true;
}

function pager_search ($sql,$db) {
	//インストールしたPEARのPagerライブラリを読み込む
	require_once('Pager/Pager.php');

	//1ページあたりに表示するデータ数
	$pagelength = "1";

	//データを格納する配列
	$data_array=array();

	//SQLを実行する
	$anq_list = $db->getAll($sql,DB_FETCHMODE_ASSOC);

	//データ数を取得する
	$total = count($anq_list);
	
	//ページャーライブラリに渡す設定（パラメーター
	$page=array(
	"itemData"=>$anq_list,  //アイテムの配列です。
	"totalItems"=>$total, //合計アイテム数
	"perPage"=>$pagelength, //１ページあたりの表示数
	"mode"=>"Jumping",
	"altFirst"=>"First", //以下、文字表示設定　１ページ目のalt表示
	"altPrev"=>"", //前のalt
	'prevImg'=>"&lt;&lt; 前を表示", //前へ　の文字表示
	"altNext"=>"", //次へ　のalt
	"nextImg"=>"次を表示 &gt;&gt;", //次へ　の文字表示
	"altLast"=>"Last", //ラストのalt表示
	"altPage"=>"",
	"separator"=>" ", //数字と数字の間の文字
	"append"=>1,
	"urlVar"=>"page",//get属性
	);

	//Pagerに設定した項目を読み込ませます
	$pager= Pager::factory($page);

	//現在のページ配列（戻り値）を取得
	$data_array["data"]=$pager->getPageData();
	//ページ遷移のリンクリストを取得
	$data_array["links"] = $pager->links;
	
	//データ配列を返す
	return $data_array;
}
?>
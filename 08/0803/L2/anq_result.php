<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//初期化関数を呼び出す
init();

//ログイン状態をチェックする関数を呼び出す
loginCheck();

//データベースに接続する関数を呼び出す
$db = db_connect();

//アンケートデータを取得する
$sql = "SELECT * FROM anq_t ORDER BY create_datetime DESC";
$anq_list = $db->getAll($sql,DB_FETCHMODE_ASSOC);

//好きな動物の「,」区切りのデータを配列データに変換する処理を行う
foreach ((array)$anq_list as $key => $value ) {
	$anq_list[$key]["animal"] = explode(",",$value["animal"]);
}

//Smartyを生成
$smarty = new MySmarty();
$smarty->assign("anq_list",$anq_list);
$smarty->assign("sex_value",getSexList());
$smarty->assign("age_value",getAgeList());
$smarty->assign("animal_value",getAnimalList());
$smarty->display("admin/anq_result.tpl");
?>